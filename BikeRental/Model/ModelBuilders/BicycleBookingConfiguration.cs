﻿using BikeRental.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BikeRental.Model.ModelBuilders
{
    public class BicycleBookingConfiguration : IEntityTypeConfiguration<BicycleBooking>
    {
        public void Configure(EntityTypeBuilder<BicycleBooking> builder)
        {
            builder.HasKey(pk => pk.RentalId);

            builder.HasOne(fk => fk.Customer);
            builder.HasOne(fk => fk.BicycleInventory);
        }
    }
}
