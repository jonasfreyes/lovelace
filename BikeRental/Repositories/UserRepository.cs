﻿using BikeRental.Model;
using BikeRental.Model.Entities;
using BikeRental.ViewModel;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BikeRental.Repositories
{
    public class UserRepository //: IUserRepository
    {
        private readonly AuthDbContext _context;
        private List<SelectListItem> _userStatus;
        public UserRepository(AuthDbContext context)
        {
            _context = context;
        }

        public async Task<RegisterViewModel> GetUsers(Guid? id)
        {
            var user = await _context.Users.AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id.ToString());

            if(user != null)
            {
                var userDisplay = new RegisterViewModel
                {
                    Email = user.Email
                };

                return userDisplay;
            }

            return null;
        }
    }
}
