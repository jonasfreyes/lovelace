﻿using BikeRental.Model.Entities;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace BikeRental.Model
{
    public interface ISeedData
    {
        void SeedDatabase(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager);
    }
}