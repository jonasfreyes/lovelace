﻿using Microsoft.AspNetCore.Identity;
using System;

namespace BikeRental.Model.Entities
{
    public class UserRole : IdentityRole<Guid>
    {
        public string Description { get; set; }
    }
}
