﻿using BikeRental.ViewModel;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BikeRental.Repositories
{
    public interface IUserRepository
    {
        Task<IEnumerable<UserViewModel>> GetUsers();

        Task<UserViewModel> GetUsers(Guid? id);

        Task<bool> UpdateUser(UserViewModel model);

        UserViewModel CreateUser();

        Task<bool> SaveUser(UserViewModel model);
    }
}