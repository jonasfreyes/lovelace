﻿using BikeRental.Model.Entities;
using BikeRental.Model.ModelBuilders;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BikeRental.Model
{
    public class AuthDbContext: IdentityDbContext
    {
        public DbSet<Customer> Customers { get; set; }
        public DbSet<BicycleInventory> BicycleInventories { get; set; }
        public DbSet<BicycleType> BicycleTypes { get; set; }
        public DbSet<BicycleBooking> BicycleRentals { get; set; }
       
        public AuthDbContext(DbContextOptions<AuthDbContext> options) : base(options)
        { 
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new CustomerConfiguration());
            modelBuilder.ApplyConfiguration(new BicycleInventoryConfiguration());
            modelBuilder.ApplyConfiguration(new BicycleTypeConfiguration());
            modelBuilder.ApplyConfiguration(new BicycleBookingConfiguration());
        }
    }
}
