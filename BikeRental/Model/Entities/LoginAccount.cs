﻿using Microsoft.AspNetCore.Identity;
using System;

namespace BikeRental.Model.Entities
{
    public class LoginAccount : IdentityUser<Guid>
    {
        public string CustomTag { get; set; }
    }
}
