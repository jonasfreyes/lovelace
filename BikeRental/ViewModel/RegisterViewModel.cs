﻿using System.ComponentModel.DataAnnotations;

namespace BikeRental.ViewModel
{
    public class RegisterViewModel
    {
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [Compare(nameof(Password),ErrorMessage = "Password and Confirm Password did not match.")]
        public string ConfirmPassword { get; set; }
        
        [EnumDataType(typeof(Roles))]
        public Roles Role { get; set; }
    }

    public enum Roles
    {
        Admin ,
        Staff
    }
}
