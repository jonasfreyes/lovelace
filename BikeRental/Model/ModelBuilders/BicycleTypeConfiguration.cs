﻿using BikeRental.Model.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BikeRental.Model.ModelBuilders
{
    public class BicycleTypeConfiguration : IEntityTypeConfiguration<BicycleType>
    {
        public void Configure(EntityTypeBuilder<BicycleType> builder)
        {
            builder.HasKey(pk => pk.BikeTypeId);

            builder.HasMany(fk => fk.BicycleInventories)
                .WithOne(fk => fk.BicycleType)
                .OnDelete(DeleteBehavior.SetNull);
        }
    }
}
